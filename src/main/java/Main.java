import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiRequestException;

public class Main {

    public static void main(String[] args) {
        ApiContextInitializer.init();

        TelegramBotsApi botsApi = new TelegramBotsApi();

        JobDetail job = JobBuilder
                .newJob(RegistrationBot.class)
                .storeDurably()
                .build();

        Trigger trigger1 = TriggerBuilder
                .newTrigger()
                .withIdentity("tomorrowTrigger", "group1")
                .usingJobData("daysLeft", 1)
                .withSchedule(CronScheduleBuilder.cronSchedule(
                        "0 0 13 30 8 ? 2017"
                ))
                .forJob(job)
                .build();

        Trigger trigger7 = TriggerBuilder
                .newTrigger()
                .withIdentity("afterWeekTrigger", "group1")
                .usingJobData("daysLeft", 7)
                .withSchedule(CronScheduleBuilder.cronSchedule(
                        "0 0 13 24 8 ? 2017"
                ))
                .forJob(job)
                .build();

        SchedulerFactory schedFact = new StdSchedulerFactory();
        try {
            Scheduler sched = schedFact.getScheduler();
            sched.start();
            sched.addJob(job, true);
            sched.scheduleJob(trigger1);
            sched.scheduleJob(trigger7);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
        try {
            botsApi.registerBot(new RegistrationBot());
        } catch (TelegramApiRequestException e) {
            e.printStackTrace();
        }
    }
}
