import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import graphs.GraphFields;
import graphs.GraphPosition;
import javafx.util.Pair;
import model.Participant;
import org.bson.Document;
import org.jfree.chart.JFreeChart;
import org.quartz.*;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.send.SendPhoto;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardRemove;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.lang.Math.toIntExact;

public class RegistrationBot extends TelegramLongPollingBot implements Job {

    private Participant participant;
    private MongoCollection<Document> collection;
    MongoCredential credential = MongoCredential.createScramSha1Credential("ecuser",
            "emotioncity",
            "RQDF43rq44".toCharArray());
    ServerAddress serverAddress = new ServerAddress("127.0.0.1", 27017);

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            String messageText = update.getMessage().getText().trim();
            long chatId = update.getMessage().getChatId();
            long userId = update.getMessage().getChat().getId();

            MongoClient mongoClient = new MongoClient(serverAddress, Collections.singletonList(credential));
            MongoDatabase database = mongoClient.getDatabase("emotioncity");
            collection = database.getCollection("participants");

            saveUserId(toIntExact(userId));
            participant = getUser(toIntExact(userId));

            if (messageText.equals("/getgraphs")) {
                sendImage(chatId);
                mongoClient.close();
                return;
            }

            if (isParticipant(participant.userId, chatId)) {
                mongoClient.close();
                return;
            }

            if (messageText.equals("/start")) {
                participant.userId = toIntExact(userId);
                if (participant.count == 0) {
                    sendMsg(chatId, "Здравствуйте. Мне от вас потребуется немного информации.", null);
                    sendMsg(chatId, "Напишите, пожалуйста, свое имя.", new ReplyKeyboardRemove());
                } else {
                    sendMsg(chatId, "Напишите, пожалуйста, свое имя.", new ReplyKeyboardRemove());
                }
                participant.count = 1;
            } else if (participant.count == 1) {
                participant.firstName = messageText;
                sendMsg(chatId, "Напишите, пожалуйста, свою фамилию.", new ReplyKeyboardRemove());
                participant.count = 2;
            } else if (participant.count == 2) {
                participant.secondName = messageText;
                askFieldOfActivity(chatId);
                participant.count = 3;
            } else if (participant.count == 3) {
                participant.fieldOfActivity = messageText;
                askCompanyPosition(chatId);
                participant.count = 4;
            } else if (participant.count == 4) {
                participant.position = messageText;
                sendMsg(chatId, "С какими целями идете на воркшоп?"
                        , new ReplyKeyboardRemove());
                participant.count = 5;
            } else if (participant.count == 5) {
                participant.target = messageText;
                sendMsg(chatId, "Спасибо!", null);
                participant.count = 6;
                sendImage(chatId);
                sendMsg(chatId, "31 августа, Гостиница «Аванта» (ул. Гоголя, 41), конференц-зал (7 этаж), в 18:00" +
                        "воркшоп \"Telegram-боты. Новый инструмент для развития Вашего бизнеса\". Пожалуйста зарегистрируйтесь на сайте: http://primbiz.com/plan/?ELEMENT_ID=650", null);
                System.out.println(participant.firstName + ", " + participant.secondName
                        + ", " + participant.userId + ", "
                        + participant.fieldOfActivity + ", " + participant.position
                        + ", " + participant.target + ", " + userId);
            }
            save(participant);
            mongoClient.close();
        }
    }

    /**
     * Получить текущего пользователя
     */
    private Participant getUser(int userId) {
        Document document = collection
                .find(Document.parse("{user_id : " + Integer.toString(userId) + "}")).first();
        if (document != null) {
            return documentToParticipant(document);
        } else {
            return new Participant();
        }
    }

    /**
     * Сохнарить userId если такого еще нет
     */
    private void saveUserId(int userId) {
        long found = collection.count(Document.parse("{user_id : " + Integer.toString(userId) + "}"));
        if (found == 0) {
            Participant participant = new Participant();
            participant.count = 0;
            participant.userId = userId;
            Document document = participantToDocument(participant);
            collection.insertOne(document);
        }
    }

    /**
     * Отправить графики пользователю
     */
    private void sendImage(long chatId) {
        FindIterable<Document> documents = collection.find();
        ArrayList<Pair<String, Integer>> fieldsOfActivity = new ArrayList<>();
        /**
         * Прохожу по коллекции документов и сохраняю пары сфер
         * деятельности (Значение, колличество повторений)
         */
        for (Document d : documents) {
            String field = d.getString("field_of_activity");
            if (field != null) {
                long count = collection.count(Filters.eq("field_of_activity", field));
                Pair<String, Integer> pair = new Pair<>(field, toIntExact(count));
                fieldsOfActivity.add(pair);
            }
        }

        /**
         * Прохожу по коллекции документов и сохраняю пары
         * должностей (Значение, колличество повторений)
         */
        ArrayList<Pair<String, Integer>> positions = new ArrayList<>();
        for (Document d : documents) {
            String position = d.getString("company_position");
            if (position != null) {
                long count = collection.count(Filters.eq("company_position", position));
                Pair<String, Integer> pair = new Pair<>(position, toIntExact(count));
                positions.add(pair);
            }
        }

        JFreeChart chart = GraphFields.createChart(GraphFields.createDataset(fieldsOfActivity));
        JFreeChart chart2 = GraphPosition.createChart(GraphPosition.createDataset(positions));
        try {
            saveToFile(chart.createBufferedImage(1200, 800), participant.userId + "fields.png");
            saveToFile(chart2.createBufferedImage(1200, 800), participant.userId + "positions.png");

            SendPhoto imageFields = new SendPhoto()
                    .setChatId(chatId)
                    .setNewPhoto(new File(participant.userId + "fields.png"))
                    .setCaption("Сферы деятельности участников");

            SendPhoto imagePositions = new SendPhoto()
                    .setChatId(chatId)
                    .setNewPhoto(new File(participant.userId + "positions.png"))
                    .setCaption("Занимаемые должности участников");

            sendPhoto(imageFields);
            sendPhoto(imagePositions);
        } catch (IOException | TelegramApiException e) {
            e.printStackTrace();
        }
    }

    /**
     * проверка, зарегистрирован ли участник
     */
    private boolean isParticipant(int userId, long chatId) {
        boolean isParticipant = false;
        Participant p = getUser(userId);
        if (p.count > 5) {
            sendMsg(chatId, "Извините, Вы уже прошли этот опрос", null);
            isParticipant = true;
        }
        return isParticipant;
    }

    /**
     * перезапись участника с измененными данными
     */
    private void save(Participant participant) {
        collection.replaceOne(Filters.eq("user_id", participant.userId),
                participantToDocument(participant));
    }

    /**
     * Документ -> Участник
     */
    private Participant documentToParticipant(Document d) {
        return new Participant(d.getString("first_name"),
                d.getString("second_name"), d.getString("field_of_activity"),
                d.getString("company_position"), d.getString("target"),
                d.getInteger("user_id"), d.getInteger("count"));
    }

    /**
     * Участник -> Документ
     */
    private Document participantToDocument(Participant p) {
        return new Document("first_name", p.firstName)
                .append("second_name", p.secondName)
                .append("field_of_activity", p.fieldOfActivity)
                .append("company_position", p.position)
                .append("target", p.target)
                .append("user_id", p.userId)
                .append("count", p.count);
    }

    /**
     * Спросить про позицию в компании
     */
    private void askCompanyPosition(long chatId) {
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow row = new KeyboardRow();

        row.add("Владелец бизнеса");
        keyboard.add(row);
        row = new KeyboardRow();

        row.add("Маркетолог");
        row.add("Продавец");
        keyboard.add(row);
        row = new KeyboardRow();

        row.add("Программист");
        row.add("Продажник");
        keyboard.add(row);

        keyboardMarkup.setKeyboard(keyboard);

        sendMsg(chatId, "Ваша позиция в компании?",
                keyboardMarkup);
    }

    /**
     * Спросить о сфере деятельности
     */
    private void askFieldOfActivity(long chatId) {
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow row = new KeyboardRow();

        row.add("Ритейл");
        row.add("Маркетинг");
        row.add("Медицина");
        keyboard.add(row);
        row = new KeyboardRow();

        row.add("Бухгалтерия");
        row.add("Строительство");
        keyboard.add(row);
        row = new KeyboardRow();

        row.add("Транспортные услуги");
        row.add("Аренда");
        keyboard.add(row);
        row = new KeyboardRow();

        row.add("Финансы");
        row.add("Туризм");
        keyboard.add(row);
        row = new KeyboardRow();

        row.add("Охрана");
        row.add("Развлечения");
        keyboard.add(row);

        row = new KeyboardRow();
        row.add("ИП");
        row.add("Другое");
        keyboard.add(row);

        keyboardMarkup.setKeyboard(keyboard);

        sendMsg(chatId, "Какая сфера деятельности вашей компании?",
                keyboardMarkup);
    }

    /**
     * Отправить сообщение
     */
    private void sendMsg(long chatId, String text, ReplyKeyboard keyboardMarkup) {
        SendMessage message = new SendMessage()
                .setChatId(chatId)
                .setReplyMarkup(keyboardMarkup)
                .setText(text);
        try {
            sendApiMethod(message);

        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    /**
     * Сохранить картинку в файл .png
     */
    private static void saveToFile(BufferedImage img, String fileName) throws IOException {
        File outputfile = new File(fileName);
        ImageIO.write(img, "png", outputfile);
    }

    private void sendBroadcastNotification(String text) {
        MongoClient mongoClient = new MongoClient(serverAddress, Collections.singletonList(credential));
        MongoDatabase database = mongoClient.getDatabase("emotioncity");
        collection = database.getCollection("participants");
        FindIterable<Document> documents = collection.find();
        for (Document document : documents) {
            Participant p = documentToParticipant(document);
            String startText;
            if (p.firstName != null) {
                startText = "Добрый день " + p.firstName + "! ";
            } else {
                startText = "Добрый день! ";
            }
            sendMsg(p.userId, startText + text, null);
        }
        mongoClient.close();
    }

    @Override
    public String getBotUsername() {
        return "PrimBizWorkshopBot";
    }

    @Override
    public String getBotToken() {
        return "446098593:AAGmUNTb8veEZF2cCDYSNyPkPUYLWOohv_A";
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        if (context.getTrigger().getJobDataMap().getInt("daysLeft") == 7){
            sendBroadcastNotification("Уже через неделю состоится долгожданный воркшоп " +
                    "\"Telegram-боты. Новый инструмент для развития Вашего бизнеса\". " +
                    "Напоминаю, 31 августа, Гостиница «Аванта» (ул. Гоголя, 41), конференц-зал " +
                    "(7 этаж), в 18:00! А если вы еще не зарегистрировались," +
                    " вот ссылка: http://primbiz.com/plan/?ELEMENT_ID=650\n" +
                    "Отличного дня!");
        } else if (context.getTrigger().getJobDataMap().getInt("daysLeft") == 1) {
            sendBroadcastNotification("Ждем вас завтра на нашем мероприятии! " +
                    "А если вы еще не зарегистрировались," +
                    " вот ссылка: http://primbiz.com/plan/?ELEMENT_ID=650\n" + "Отличного дня!");
        }
    }
}