package model;

public class Participant {
    public String firstName;
    public String secondName;
    public String fieldOfActivity;
    public String position;
    public String target;
    public int userId;
    public int count;

    public Participant() {
    }

    public Participant(String firstName, String secondName, String fieldOfActivity, String position,
                       String target, int userId, int count) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.fieldOfActivity = fieldOfActivity;
        this.position = position;
        this.target = target;
        this.userId = userId;
        this.count = count;
    }
}
